# BCM Test

## What's left:

* Improve code coverage
* I didn't do any logging only some comment, but I choose to let the Runner handle all the exceptions that could occur.
* I used Apache commons CLI to handle the CLI arguments but due to the lack of time not for all the args.
* I didn't fully used Apache commons CLI to display help messages and handle missing args
* The unit tests should be using relative path but I had problems with that so I skipped this part.
* I haven't really used git, so I didn't do any gitignore

## Building

````
mvn clean install
java -jar bcm-test-0.0.1-SNAPSHOT-jar-with-dependencies.jar -f /home/constant/Projets/bcm-data/context/coordinates_regions.csv -u https://my.api.mockaroo.com/sensor/temperatures -a dd764f40
````

## Scalability

In order to have a scalable solution I would use a queue.

The collect part will call the API to collect sensor data, if we have huge quantity of sensor data the API will probably offer some sort of paging. Using the paging we would be able to have multiple collect agent that will insert the data in a JMS queue, like RabbitMQ or ActiveMQ.

Then we will have multiples matching agents that will collect the messages in the queue and exploit them. The process will be, find the region containing the sensor and insert it in a distributed database.

We have two choices on the type of data we want to insert:
 * insert directly the sensor data -> this way we will keep all the raw data, in case in future time we want to be able to aggregate the data on a different granularity, in this case it could be a good idea to go with a solution like PostGis.
 * perform the matching of the temperature with a region, then insert it, the data would look like this: sensor_date, region, auto_inc, temperature
 
In either case the database will be in charge of the aggregation calculation.

````
   +---------------------------------------------------------------------------------------------+
   |   +-------------+                                                                           |
   |   |             |                                                                           |
   |   |  Every hour |                                                                           |
   |   |             |                                                                           |
   |   +-------------+                                                                           |
   |                                                                                             |
   |  +------------------------+    Adding each element     +-------------+                      |
   |  |                        |                            |             |                      |
   |  | Collecting sensor data +--------------------------->+  JMS QUEUE  |                      |
   |  |                        |                            |             |                      |
   |  +------------------------+                            +-------------+                      |
   |                                                               |                             |
   |                                                         +-----------------+                 |
   |                                                         |                 |                 |
   |                                                         |                 |                 |
   |                                           +-------------v-----+       +---v---------------+ |
   |                                           | Matching agent #1 |       | Matching agent #? | |
   |                                           +-------------------+       +-------------------+ |
   |                                                                                             |
   +---------------------------------------------------------------------------------------------+
````

## Operations

In order to have a good monitoring of the whole process I like to use Logstash or/and Filebeat to collect logs and store them in Elasticsearch. We will then be able to monitor the whole process using a Kibana dashboard. 

For the scheduling of the task I want to use CRON, this way the code of the agent doesn't have to handle the fail and retry of the job.
If one of the execution fails, the kibana dashboard will display it and the job will retry in one hour.

About versioning I don't really understand what is expected. 

## SQL Query

````
SELECT t.region_name, t.survey_date, t.average_temperature, t3.average_temperature 
FROM TEMPERATURES t INNER JOIN TEMPERATURES t3 ON t.region_name = t3.region_name AND t3.survey_date = DATE_SUB(t.survey_date, INTERVAL 75 SECOND)
WHERE t.survey_date = (SELECT MAX(t2.survey_date) from TEMPERATURES t2);
````

````
+-----------------------------+---------------------+---------------------+---------------------+
| region_name                 | survey_date         | average_temperature | average_temperature |
+-----------------------------+---------------------+---------------------+---------------------+
| Bretagne                    | 2019-03-03 15:31:25 |   28.75659523809524 |  29.027333333333335 |
| Rhône-Alpes                 | 2019-03-03 15:31:25 |  27.726822077922076 |  27.398644827586207 |
| Champagne-Ardenne           | 2019-03-03 15:31:25 |  27.311263157894736 |  27.783985714285716 |
| Bourgogne                   | 2019-03-03 15:31:25 |  30.063345833333333 |         28.79030625 |
| Basse-Normandie             | 2019-03-03 15:31:25 |   28.65063888888889 |  28.831176190476192 |
| Haute-Normandie             | 2019-03-03 15:31:25 |  30.062888235294118 |  28.630089473684208 |
| Alsace                      | 2019-03-03 15:31:25 |  27.429242857142857 |  27.178559090909093 |
| Poitou-Charentes            | 2019-03-03 15:31:25 |  27.845394339622644 |  28.679777083333335 |
| Auvergne                    | 2019-03-03 15:31:25 |   29.30905909090909 |   26.95038095238095 |
| Franche-Comté               | 2019-03-03 15:31:25 |  28.432342857142856 |  28.090819999999997 |
| Lorraine                    | 2019-03-03 15:31:25 |   29.98192608695652 |           27.735528 |
| Centre                      | 2019-03-03 15:31:25 |   28.64362935323383 |          28.2242375 |
| Pays de la Loire            | 2019-03-03 15:31:25 |  27.620507407407406 |   27.69408333333333 |
| Provence-Alpes-Côte d'Azur  | 2019-03-03 15:31:25 |   27.78275588235294 |   27.79881797752809 |
| Picardie                    | 2019-03-03 15:31:25 |   27.80810172413793 |   27.73961888888889 |
| Aquitaine                   | 2019-03-03 15:31:25 |  28.637951282051283 |   28.09157111111111 |
| Nord-Pas-de-Calais          | 2019-03-03 15:31:25 |          27.6051375 |           28.505384 |
| Languedoc-Roussillon        | 2019-03-03 15:31:25 |    28.5549987654321 |  28.858757500000003 |
| Midi-Pyrénées               | 2019-03-03 15:31:25 |  31.817957142857146 |          29.3569125 |
| Limousin                    | 2019-03-03 15:31:25 |  29.426000000000002 |            29.54275 |
| Corse                       | 2019-03-03 15:31:25 |             24.5767 |             32.0338 |
+-----------------------------+---------------------+---------------------+---------------------+
````
````
+-------------+---------------------+---------------------+-----------------+
| region_name | survey_date         | average_temperature | survey_quantity |
+-------------+---------------------+---------------------+-----------------+
| Bretagne    | 2019-03-03 14:57:10 |            28.72843 |              50 |
| Bretagne    | 2019-03-03 15:08:21 |  28.847171428571432 |              42 |
| Bretagne    | 2019-03-03 15:23:07 |  28.488443181818184 |              44 |
| Bretagne    | 2019-03-03 15:27:29 |   26.60653947368421 |              38 |
| Bretagne    | 2019-03-03 15:30:10 |  29.027333333333335 |              57 |
| Bretagne    | 2019-03-03 15:31:25 |   28.75659523809524 |              42 |
+-------------+---------------------+---------------------+-----------------+
````
