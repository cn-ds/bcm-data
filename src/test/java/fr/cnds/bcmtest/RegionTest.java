package fr.cnds.bcmtest;

import org.junit.Assert;
import org.junit.Test;

public class RegionTest {
	
	@Test
	public void testInitializationOfRegionBoundingBox() {
		Region region = new Region("testRegion");
		Point pointSO = new Point(0D,0D);
		Point pointNO = new Point(1D,0D);
		Point pointNE = new Point(1D,1D);
		Point pointSE = new Point(0D,1D);
		region.addPoint(pointSO);
		region.addPoint(pointSE);
		region.addPoint(pointNE);
		region.addPoint(pointNO);
		Assert.assertEquals(pointNE, region.getRegionLimits().get(0));
		Assert.assertEquals(pointSE, region.getRegionLimits().get(1));
		Assert.assertEquals(pointSO, region.getRegionLimits().get(2));
		Assert.assertEquals(pointNO, region.getRegionLimits().get(3));
	}
	
	@Test
	public void testBoudariesOfRegionBoundingBox() {
		Region region = new Region("testRegion");
		Point point1 = new Point(100.50D,/*maxLong*/120.50D);
		Point point2 = new Point(/*maxLat*/230.54D,/*minLong*/-225.0D);
		Point point3 = new Point(/*minLat*/58D,33D);
		Point point4 = new Point(215D,111.5D);
		Point point5 = new Point(218.54D,5.0D);
		region.addPoint(point1);
		region.addPoint(point2);
		region.addPoint(point3);
		region.addPoint(point4);
		region.addPoint(point5);
		Assert.assertEquals(new Point(230.54D, 120.50D), region.getRegionLimits().get(0));
		Assert.assertEquals(new Point(58D, 120.50D), region.getRegionLimits().get(1));
		Assert.assertEquals(new Point(58D, -225.0D), region.getRegionLimits().get(2));
		Assert.assertEquals(new Point(230.54D, -225.0D), region.getRegionLimits().get(3));
	}
	
	@Test
	public void testContainsFunction() {
		Region region = new Region("testRegion");
		Point pointSO = new Point(-10D,-10D);
		Point pointNO = new Point(10D,-10D);
		Point pointNE = new Point(10D,10D);
		Point pointSE = new Point(-10D,10D);
		region.addPoint(pointSO);
		region.addPoint(pointSE);
		region.addPoint(pointNE);
		region.addPoint(pointNO);
		Assert.assertTrue(region.contains(new Point(0D,0D)));
		Assert.assertFalse(region.contains(new Point(-11D,0D)));
	}
}
