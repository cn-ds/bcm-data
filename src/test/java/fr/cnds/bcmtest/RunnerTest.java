package fr.cnds.bcmtest;

import org.junit.Test;

import fr.cnds.bcmtest.runner.Runner;

public class RunnerTest {
	
	@Test
	public void runnerStandardCase() {
		String[] args = new String[3];
		args[0] = "-f /home/constant/Projets/bcm-data/context/coordinates_regions.csv";
		args[1] = "-u https://my.api.mockaroo.com/sensor/temperatures";
		args[2] = "-a dd764f40";
		RegionFileParser rfp = new RegionFileParser();
		Runner.main(args);
		System.out.println(rfp.toString());
	}
}
