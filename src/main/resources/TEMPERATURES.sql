CREATE TABLE TEMPERATURES (
	region_name VARCHAR(30),
	survey_date TIMESTAMP,
	average_temperature DOUBLE,
	survey_quantity INT,
	KEY(region_name, survey_date));