package fr.cnds.bcmtest;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 * Class used to parse the csv file with the different cities
 */
public class RegionFileParser {

	private Map<String, Region> regionCoordinates = new HashMap<>();

	public Map<String, Region> parse(CSVParser parser) {
		boolean firstLine = true;
		@SuppressWarnings("unused") // We should use errorNb to log something
		int errorNb = 0;
		for (CSVRecord record : parser) {
			if (firstLine) {
				firstLine = false;
				continue;
			}
			final String regionName = record.get(2);
			try {
				final Double latitude = Double.valueOf(record.get(11));
				final Double longitude = Double.valueOf(record.get(12));
				Region region = regionCoordinates.get(regionName);
				if (region == null) {
					region = new Region(regionName);
					regionCoordinates.put(regionName, region);
				}
				region.addPoint(new Point(latitude, longitude));
			} catch (NumberFormatException nfe) {
				errorNb++;
			}
		}
		// TODO : log something about the errors
		return regionCoordinates;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		regionCoordinates.forEach((regionName, region) -> {
			stringBuilder.append(region.toString() + "\n");
		});
		return "RegionFileParser [Regions=" + stringBuilder.toString() + "]";
	}

}
