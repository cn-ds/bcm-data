package fr.cnds.bcmtest;

/**
 * Represents a point on the world defined by a latitude and a longitude.
 */
public class Point {
	// latitude and longitude are public to avoid using useless getter and setter
	public Double latitude;
	public Double longitude;
	
	public Point(Double latitude, Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Point [" + latitude + ", " + longitude + "]";
	}
}
