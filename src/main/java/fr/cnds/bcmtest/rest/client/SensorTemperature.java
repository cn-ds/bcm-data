package fr.cnds.bcmtest.rest.client;

import fr.cnds.bcmtest.Point;

/**
 * POJO representing one of the object returned by the API call
 *
 */
public class SensorTemperature {
	private String timestamp;
	private Point localisation;
	private Double temperature;
	private String country;
	
	public SensorTemperature(String timestamp, Point localisation, Double temperature, String country) {
		super();
		this.timestamp = timestamp;
		this.localisation = localisation;
		this.temperature = temperature;
		this.country = country;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Point getLocalisation() {
		return localisation;
	}

	public void setLocalisation(Point localisation) {
		this.localisation = localisation;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
