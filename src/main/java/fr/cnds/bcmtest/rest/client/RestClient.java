package fr.cnds.bcmtest.rest.client;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import fr.cnds.bcmtest.Point;

public class RestClient {
	private String apiKey;
	private String url;
	
	/* X-API-Key header used to send api credential */
	private final String X_API_KEY = "X-API-Key";

	public RestClient(String apiKey, String url) {
		this.apiKey = apiKey;
		this.url = url;
	}

	/**
	 * Call the web service, parse the temperatures and return them. 
	 * @return a list of sensor temperatures
	 * @throws UnirestException if the API is not reachable
	 */
	public List<SensorTemperature> retrieveTemperatures() throws UnirestException {
		HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
				.header(X_API_KEY, apiKey)
				.asJson();
		List<SensorTemperature> sensorTemperatures = new ArrayList<SensorTemperature>();
		jsonResponse.getBody().getArray().forEach( survey -> {
			JSONObject surveyObject = (JSONObject) survey;
			try {
				SensorTemperature temperature = new SensorTemperature(
						surveyObject.getString("timestamp"),
						new Point(surveyObject.getDouble("lat"), surveyObject.getDouble("lon")),
						surveyObject.getDouble("temperature"),
						surveyObject.getString("country"));
				sensorTemperatures.add(temperature);
			} catch (Exception e) {
				//TODO handle this exception, sometimes the temperature is "null".
			}
		});
		return sensorTemperatures;
	}

}
