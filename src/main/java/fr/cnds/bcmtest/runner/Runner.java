package fr.cnds.bcmtest.runner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import com.mashape.unirest.http.exceptions.UnirestException;

import fr.cnds.bcmtest.Region;
import fr.cnds.bcmtest.RegionFileParser;
import fr.cnds.bcmtest.RegionTemperatures;
import fr.cnds.bcmtest.rest.client.RestClient;
import fr.cnds.bcmtest.rest.client.SensorTemperature;

public class Runner {
	
	private static String regionFile = "";
	private static String url = ""; 
	private static String apiKey = "";
	private static String database = "";
	private static String dbUrl = "";
	private static String username = "";
	private static String password = "";
	
	public static void main(String[] args) {
		parseArgs(args);
		final File csvData = new File(regionFile);
		CSVParser parser = null;
		try {
			//Parse region description file
			parser = CSVParser.parse(csvData, StandardCharsets.UTF_8, CSVFormat.newFormat(';'));
			RegionFileParser regionFileParser = new RegionFileParser();
			Map<String, Region> regions = regionFileParser.parse(parser);
			//Retrieve sensor data
			RestClient restClient = new RestClient(apiKey, url);
			List<SensorTemperature> temperatures = restClient.retrieveTemperatures();
			//Aggregation and insertion in database
			RegionTemperatures regionTemperatures = new RegionTemperatures(temperatures, regions);
			regionTemperatures.persistTemperatures(dbUrl, database, username, password);
		} catch (IOException e) {
			// TODO Handle the log message
			e.printStackTrace();
			return;
		} catch (UnirestException e) {
			// TODO Handle the log message
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Handle the log message
			e.printStackTrace();
		} finally {
			RegionTemperatures.closeConnection();
		}
	}
	
	/**
	 * Parse all the args and fill them
	 * @param args: arguments send to the jar
	 */
	private static void parseArgs(String[] args) {
		Options options = new Options();
		options.addOption(new Option("f", "regionfile", true, "File containing region description"));
		options.addOption(new Option("u", "url", true, "Api url"));
		options.addOption(new Option("a", "apikey", true, "Api key"));
		options.addOption(new Option("d", "database", false, "Database url"));
		options.addOption(new Option("n", "db-name", false, "Database name"));
		options.addOption(new Option("r", "username", false, "Database username"));
		options.addOption(new Option("p", "password", false, "Database password"));
		CommandLineParser cliParser = new DefaultParser();
	    try {
	        CommandLine line = cliParser.parse( options, args );
	        regionFile = "/home/constant/Projets/bcm-data/context/coordinates_regions.csv";//line.getOptionValue("f");
	        url = line.getOptionValue("u");
	        apiKey = line.getOptionValue("a");
	        dbUrl = "35.195.64.14:3306";
	        database = "interview";
	        username = "candidate";
	        password = "aIhsE6yS7exq7yz";
	    } catch( ParseException exp ) {
	        // oops, something went wrong
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	    }
	}
}
