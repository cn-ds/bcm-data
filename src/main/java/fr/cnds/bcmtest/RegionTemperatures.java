package fr.cnds.bcmtest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.cnds.bcmtest.rest.client.SensorTemperature;

public class RegionTemperatures {

	private static Connection connection = null;
	private Map<String, List<SensorTemperature>> regionSurveys = new HashMap<String, List<SensorTemperature>>();

	public RegionTemperatures(List<SensorTemperature> temperatures, Map<String, Region> regions) {
		// Browse every survey
		temperatures.forEach(temperature -> {
			// Test for each region if the survey was taken inside of it
			for (String regionName: regions.keySet()) {
				Region region = regions.get(regionName);
				if (region.contains(temperature.getLocalisation())) {
					// Link the sensor temperature to the region
					List<SensorTemperature> sensorTemperatures = regionSurveys.get(regionName);
					if (sensorTemperatures == null) {
						sensorTemperatures = new ArrayList<>();
						regionSurveys.put(regionName, sensorTemperatures);
					}
					sensorTemperatures.add(temperature);
					break;
				}
			}
		});
		// TODO log the size of parsed elements
	}

	public void persistTemperatures(String url, String db, String username, String password) throws SQLException {
		initDatabaseConnection(url, username, password, db);
		Timestamp currentTime = Timestamp.valueOf(LocalDateTime.now());
		PreparedStatement preparedStatement = connection.prepareStatement(
			"insert into TEMPERATURES("
			+ "region_name,survey_date,average_temperature,survey_quantity"
			+ ") values (?,?,?,?)");
		for (String regionName : regionSurveys.keySet()) {
			List<SensorTemperature> sensors = regionSurveys.get(regionName);
			final int surveyQuantity = sensors.size();
			// Calculate the sum of the temperature for a region
			final Double total = sensors.stream().mapToDouble(SensorTemperature::getTemperature).sum();
			preparedStatement.setString(1, regionName);
			preparedStatement.setTimestamp(2, currentTime);
			preparedStatement.setDouble(3, total/surveyQuantity);
			preparedStatement.setInt(4, surveyQuantity);
			preparedStatement.addBatch();
		}
		preparedStatement.executeBatch();
		closeConnection();
	}
	
	private void initDatabaseConnection(String url, String user, String password, String database) {
		String fullUrl = "jdbc:mysql://" + url + "/" + database;
		try {
		    connection = DriverManager.getConnection(fullUrl, user, password );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}
	
	public static void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				//TODO the connection was probably already closed
			}
		}
	}
}
