package fr.cnds.bcmtest;

import java.util.ArrayList;
import java.util.List;

public class Region {
	private String name; /* name of the region */
	
	private Double maxLatitude;
	private Double minLatitude;
	private Double maxLongitude;
	private Double minLongitude;
	
	private Boolean isFirstPoint = true;
	
	public Region(String name) {
		this.name = name;
	}
	
	public void addPoint(Point point) {
		if (isFirstPoint) {
			maxLatitude = minLatitude = point.latitude;
			maxLongitude = minLongitude = point.longitude;
			isFirstPoint = false;
		}
		if (point.latitude > maxLatitude) {
			maxLatitude = point.latitude;
		}
		if (point.latitude < minLatitude) {
			minLatitude = point.latitude;
		}
		if (point.longitude > maxLongitude) {
			maxLongitude = point.longitude;
		}
		if (point.longitude < minLongitude) {
			minLongitude = point.longitude;
		}
	}
	
	/**
	 * As a region is represented as a square this function returns the 4 points
	 * on each of the angles of the square
	 * @return a list of points ordered like this [NE, SE, SO, NO]
	 */
	public List<Point> getRegionLimits() {
		Point pointNE = new Point(maxLatitude, maxLongitude);
		Point pointSE = new Point(minLatitude, maxLongitude);
		Point pointSO = new Point(minLatitude, minLongitude);
		Point pointNO = new Point(maxLatitude, minLongitude);
		List<Point> points = new ArrayList<Point>();
		points.add(pointNE);
		points.add(pointSE);
		points.add(pointSO);
		points.add(pointNO);
		return points;
	}

	/**
	 * @param point, the point we want to test
	 * @return whether or not a point is inside the region
	 */
	public boolean contains(Point point) {
		if (point.latitude > minLatitude 
			&& point.latitude < maxLatitude 
			&& point.longitude > minLongitude
			&& point.longitude < maxLongitude) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		List<Point> points = getRegionLimits();
		return "Region [name=" + name 
				+ ",\nNE=" + points.get(0) 
				+ ",\nSE=" + points.get(1) 
				+ ",\nSO=" + points.get(2) 
				+ ",\nNO=" + points.get(3)
				+ ",]";
	}
}
